import os
from sqlalchemy import create_engine
from sqlalchemy.engine.url import URL
import pandas as pd


class DatabaseHandler:
    def __init__(self):
        self.__engine = self.create_database_engine()

    def create_database_url(self):
        """Returns the URI link to the InsideEdge database."""
        user = os.getenv("DB_USER", False)
        port = os.getenv("DB_PORT", False)
        password = os.getenv("DB_PASSWORD", False)
        host = os.getenv("DB_HOST", False)
        database = os.getenv("DB_NAME", False)
        if not all([user, port, password, host, database]):
            raise Exception((
                "Missing database environment variable."
                " Please make sure the user, port, password, host and"
                " database environment variables were set before"
                " running the script."
            ))

        return URL(
            drivername="postgres",
            username=user,
            password=password,
            host=host,
            port=port,
            database=database
        )

    def create_database_engine(self):
        """Returns an sqlalchemy engine instance to the InsideEdge db."""
        try:
            inside_edge_db_url = self.create_database_url()
            return create_engine(inside_edge_db_url)
        except Exception as e:
            raise Exception(e)

    def fetch_query_results(self, query):
        """Fetches the query results from the database and returns the results
        as a pandas dataframe.

        query: str - Query string to execute.
        """
        try:
            with self.__engine.connect() as conn:
                results_proxy = conn.execute(query)
            return pd.DataFrame([dict(row) for row in results_proxy])
        except Exception as e:
            raise Exception(e)
