import os
import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn.metrics import r2_score
from statsmodels.stats.outliers_influence import variance_inflation_factor    
from statsmodels.tools.tools import add_constant
from database import DatabaseHandler
import argparse

# Read in command line arguments
def parseArgs():
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--debug', action='store_true', help='If set, run in debug mode')
    return parser.parse_args()

# Calculate the spray angle of a batted ball
# NOTE: For left handed batters return the inverse to keep all values on the same scale
def getBattedBallDirection(hc_x, hc_y, stand):
    try:
        phi = math.degrees(math.atan((hc_x-125.42)/(198.27-hc_y)))
    except:
        phi = 0
    if stand == 'L':
        phi *= -1
    return phi

# Get the percentage of plate appearences in each ballpark for a given hitter's season
def paPercents(d, paCount):
    newDict = dict()
    for x in d.keys():
            newDict[x] = float(d[x])/paCount
    return newDict

# Calculate HR park factor for a given batter's season
# HR park factors are from Dan Richard's article (https://www.pitcherlist.com/going-deep-barrels-and-ballpark-factors-pt-ii/)
def getParkFactor(seasonDf, pfDf, centerPct, pullPct):
    pfCols = ['Park', 'PullBrlHrRate', 'ParkFactor']

    lhPf = pfDf[['Park', 'LHH Pull HR/Brl', 'LHH Park Factor']]
    rhPf = pfDf[['Park', 'RHH Pull HR/Brl', 'RHH Park Factor']]
    centerPf = pfDf[['Park', 'Center HR/Brl', 'Center Park Factor']]

    lhPf.columns = rhPf.columns = centerPf.columns = pfCols

    paCount = len(seasonDf)
    lhDf = seasonDf[seasonDf.batter_handedness == 'L']
    rhDf = seasonDf[seasonDf.batter_handedness == 'R']

    lhPctFactor, lhZFactor = getDirectionalParkFactor(lhDf, lhPf, paCount)
    rhPctFactor, rhZFactor = getDirectionalParkFactor(rhDf, rhPf, paCount)
    centerPctFactor, centerZFactor = getDirectionalParkFactor(seasonDf, centerPf, paCount)

    pctFactor = (pullPct * lhPctFactor) + (pullPct * rhPctFactor) + (centerPct * centerPctFactor)
    zFactor = (pullPct * lhZFactor) + (pullPct * rhZFactor) + (centerPct * centerZFactor)

    return pctFactor, zFactor

# Gets HR park factor for a given batted ball direction 
def getDirectionalParkFactor(df, pfDf, paCount):
    pctFactor = 0
    zFactor = 0
    paPct = paPercents(dict(df.home_team_abbreviation.value_counts()), paCount)
    for park in paPct.keys():
        factors = pfDf[pfDf.Park == park].iloc[0]
        pctFactor += factors.PullBrlHrRate * paPct[park]
        zFactor += factors.ParkFactor * paPct[park]
    return pctFactor, zFactor

def getPaParkFactor(hitX, hitY, side, park, pfDf):
    centerPf = pfDf[pfDf.Park == park].iloc[0]['Center Park Factor']
    if side == 'R':
        pullPf = pfDf[pfDf.Park == park].iloc[0]['RHH Park Factor']
    else:
        pullPf = pfDf[pfDf.Park == park].iloc[0]['LHH Park Factor']

    sprayAngle = getBattedBallDirection(hitX, hitY, side)

    if (sprayAngle < -15):
        return pullPf
    elif (sprayAngle > 15):
        return 0
    else:
        return centerPf

# Calculates variable inflation factor for a set independant variables and
# removing the one with the highest value until all are under the given threshold 
def calculate_vif(df, thresh=5.0):
    X = add_constant(df)
    variables = list(range(X.shape[1]))    
    dropped = True
    while dropped:
        dropped = False
        vif = [variance_inflation_factor(X.iloc[:, variables].values, ix)
               for ix in range(X.iloc[:, variables].shape[1])]
        print(vif)
        maxloc = vif.index(max(vif[1:]))
        if max(vif[1:]) > thresh:
            print('dropping \'' + X.iloc[:, variables].columns[maxloc] +
                  '\' at index: ' + str(maxloc))
            del variables[maxloc]
            dropped = True

    print('Remaining variables:')
    print(X.columns[variables[1:]])
    return X.iloc[:, variables[1:]]

# Calculate agjusted r^2
def adjustedR2(data, r2):
    n, k = data.shape
    return 1 - (((1 - r2) * (n - 1)) / (n - k - 1))

# Get the results of all plate appearences
def getPlateAppearenceResults(debug):
    # If running in debug mode and csv dataset exists, use that
    # Otherwise get the data from the database and store to in a csv if in debug mode
    if False and debug and os.path.exists("paResults.csv"):
        print("Found pa results csv.")
        paResults = pd.read_csv("paResults.csv")
        paResults.game_date = pd.to_datetime(paResults.game_date)
    else:
        dh = DatabaseHandler()
        paResults = dh.fetch_query_results(
            """SELECT matchups.id, batter_id, players.full_name, games.game_date, \
            games.home_team_abbreviation, event_name, batter_handedness, hit_coordinate_x, \
            hit_coordinate_y, batted_ball_type, launch_speed_angle_code, is_hit_into_play, \
            is_homerun, is_plate_appearance, is_strikeout, is_walk FROM matchups INNER JOIN \
            players ON matchups.batter_id = players.id INNER JOIN games on matchups.game_id = games.id \
            WHERE is_plate_appearance = true"""
        )
        if debug:
            paResults.to_csv("paResults.csv", index=False)

    # Create a column for the season the PA occured in and fill in any NAs in the dataset with a 0
    paResults['season'] = paResults.apply(lambda x: x.game_date.year, axis=1)
    paResults = paResults.fillna(0)

    return paResults

# Create a pandas dataframe of variables used by the model for each player's season
def calculateData(debug):
    parkFactors = pd.read_csv("HomeRunParkFactors.csv")
    
    paResults = getPlateAppearenceResults(debug)

    # Create a column for the season the PA occured in and fill in any NAs in the dataset with a 0
    paResults['season'] = paResults.apply(lambda x: x.game_date.year, axis=1)
    paResults = paResults.fillna(0)

    # Get unique lists of batter ids and seasons
    batters = list(set(paResults.batter_id))
    seasons = list(set(paResults.season))

    # Initialize output dataframe
    batterData= pd.DataFrame()

    # For each hitter calculate their season statistics for each season there's data for
    for batter in batters:
        batterDf = paResults[paResults.batter_id == batter]
        name = batterDf.iloc[0].full_name
        if debug:
            print(name)
        for season in seasons:
            seasonDf = batterDf[batterDf.season == season]            
            paCount = len(seasonDf)
            if paCount == 0:
                continue
            seasonDf['SprayAngle'] = seasonDf.apply(lambda x: getBattedBallDirection(float(x.hit_coordinate_x), float(x.hit_coordinate_y), x.batter_handedness), axis=1)
            bbe = len(seasonDf[seasonDf.is_hit_into_play == True])
            barrels = len(seasonDf[(seasonDf.launch_speed_angle_code == 6)])
            strikeouts = len(seasonDf[seasonDf.is_strikeout == True])
            walks = len(seasonDf[seasonDf.is_walk == True])
            lineDrives = len(seasonDf[seasonDf.batted_ball_type == 'line_drive'])
            flyBalls = len(seasonDf[seasonDf.batted_ball_type == 'fly_ball'])
            homeRuns = len(seasonDf[seasonDf.is_homerun == True])
            pulledDf = seasonDf[seasonDf.SprayAngle <= -15]
            straightawayDf = seasonDf[(seasonDf.SprayAngle > -15) & (seasonDf.SprayAngle < 15)]
            pulledBalls = len(pulledDf)
            straightawayBalls = len(straightawayDf)
            pulledLineDrives = len(pulledDf[pulledDf.batted_ball_type == 'line_drive'])
            pulledFlyBalls = len(pulledDf[pulledDf.batted_ball_type == 'fly_ball'])
            pulledBarrelsDf = pulledDf[(pulledDf.launch_speed_angle_code == 6)]
            pulledBarrels = len(pulledBarrelsDf)
            pulledFlyBallBarrels = len(pulledBarrelsDf[pulledBarrelsDf.batted_ball_type == 'fly_ball'])
            pctParkFactor, parkFactor = getParkFactor(seasonDf, parkFactors, pulledBalls/paCount, straightawayBalls/paCount)

            data = {
                'Batter': name,
                'Id': batter,
                'Season': season,
                'PA': paCount,
                'BBE': bbe,
                'K': strikeouts,
                'BB': walks,
                'LDs': lineDrives,
                'FBs': flyBalls,
                'HRs': homeRuns,
                'Brls': barrels,
                'PulledBalls': pulledBalls,
                'StraightawayBalls': straightawayBalls,
                'PulledLDs': pulledLineDrives,
                'PulledFBs': pulledFlyBalls,
                'PulledBrls': pulledBarrels,
                'PulledFBBrls': pulledFlyBallBarrels,
                'PercentHomeRunParkFactor': pctParkFactor,
                'HomeRunParkFactor': parkFactor
            }

            batterData = batterData.append(data, ignore_index=True)

    return batterData[[
        'Batter',
        'Id',
        'Season',
        'PA',
        'BBE',
        'K',
        'BB',
        'LDs',
        'FBs',
        'HRs',
        'Brls',
        'PulledBalls',
        'StraightawayBalls',
        'PulledLDs',
        'PulledFBs',
        'PulledBrls',
        'PulledFBBrls',
        'PercentHomeRunParkFactor',
        'HomeRunParkFactor'
    ]].sort_values(by=['Batter', 'Season'])

def runBySeasonModel(args):
    # Read in data
    # If running in debug mode try to read data from csv otherwise save the data to a csv
    if args.debug and os.path.exists("Data.csv"):
        data = pd.read_csv("Data.csv")
    else:
        data = calculateData(args.debug)
        if args.debug:
            data.to_csv("Data.csv")    

    # Remove low PA seasons
    data = data[data.PA >= 200]

    # Create a unique id for each individual season for each player
    data['SeasonId'] = data.apply(lambda row: str(row.Id) + '_' + str(row.Season), axis=1)

    # Create a dictionary mapping for the number of plate appearences a batter had in a given season
    paCountDict = dict(zip(data.SeasonId, data.PA))

    # Get the data for the independant variables
    X = pd.DataFrame()
    X['Brls/PA%'] = data.Brls / data.PA
    X['Pulled FBBrl%'] = data.PulledFBBrls / data.PA
    X['Park Factor Z-Score'] = data.HomeRunParkFactor

    # Remove variables that have high multicollinearity
    X = calculate_vif(X)

    # Get the dependant variable
    Y = (data.HRs / data.PA).values

    # Create dataframe for model output
    predictionDf = pd.DataFrame(columns=[
        'Player', 'Id', 'Season', 'Park Factor', 'PA', 'HR', 'HR%', 'pHR', 'pHR%', 'adjpHR', 'adjpHR%'
    ])

    # Add all of the static data to output dataframe
    predictionDf.Player = data.Batter
    predictionDf.Id = data.Id
    predictionDf['Season'] = data.Season
    predictionDf['Park Factor'] = data.HomeRunParkFactor
    predictionDf['PA'] = data.PA
    predictionDf['HR'] = data.HRs
    predictionDf['HR%'] = data.HRs / data.PA

    # Set the variables for each type of model
    modelTypes = {
        'pHR': ['Brls/PA%', 'Pulled FBBrl%'],
        'adjpHR': ['Brls/PA%', 'Pulled FBBrl%', 'Park Factor Z-Score']
    }

    # If debugging run each model 10,000 times and take the average r2 of each result
    if args.debug:
        for modelType in modelTypes.keys():
            vars = modelTypes[modelType]
            print(vars)
            X_Filtered = X[vars].values
                
            scores = list()
            adjustedScores = list()

            for _ in range(10000):
            # Split data into test and train sets
                X_train, X_test, Y_train, Y_test = train_test_split(X_Filtered, Y, shuffle=True, test_size=0.2)

                # Create and train model
                model = LinearRegression()
                model.fit(X_train, Y_train)

                # Test model
                score = (model.score(X_test, Y_test))
                scores.append(score)
                adjustedScores.append(adjustedR2(X_test, score))
            print("Average R2 {}".format(np.mean(scores)))
            print("Average Adjusted R2 {}".format(np.mean(adjustedScores)))

    # Create models using all of the data and make predictions
    for modelType in modelTypes.keys():
        vars = modelTypes[modelType]
        X_Filtered = X[vars].values
        model = LinearRegression()
        model.fit(X_Filtered, Y)
        predicted = dict(zip(data.SeasonId, model.predict(X_Filtered)))
        predictionDf[modelType + '%'] = predicted.values()
        predictionDf[modelType] = [paCountDict[key] * predicted[key] for key in predicted.keys()]
    
    # Save the output to a csv
    predictionDf.to_csv(os.path.join(os.path.dirname(os.path.realpath(__file__)), "pHR_Predictions.csv"), index=False)

def runByPlateAppearenceModel(args):
    # Get plate appearence results
    paResults = getPlateAppearenceResults(args.debug)

    # Get park factors
    parkFactors = pd.read_csv("HomeRunParkFactors.csv")

    bbe = paResults[paResults.is_hit_into_play == True]

    # Get the data for the independant variables
    X = pd.DataFrame()
    X['IsFlyBall'] = bbe.apply(lambda x: x.batted_ball_type == 'fly_ball', axis=1).astype(int)
    X['SprayAngle'] = bbe.apply(lambda x: getBattedBallDirection(float(x.hit_coordinate_x), float(x.hit_coordinate_y), x.batter_handedness), axis=1)
    X['IsBarrel'] = bbe.apply(lambda x: x.launch_speed_angle_code == 6, axis=1).astype(int)
    X['ParkFactor'] = bbe.apply(lambda x: getPaParkFactor(float(x.hit_coordinate_x), float(x.hit_coordinate_y), x.batter_handedness, x.home_team_abbreviation, parkFactors), axis=1)

    # Remove variables that have high multicollinearity
    X = calculate_vif(X)

    # Get the dependant variable
    Y = bbe.is_homerun.astype(int)

    # Create dataframe for model output
    predictionDf = pd.DataFrame(columns=[
        'Player', 'Id', 'Season', 'Park Factor', 'IsHomeRun', 'pHR', 'adjpHR'
    ])

    # Add all of the static data to output dataframe
    predictionDf.Player = bbe.full_name
    predictionDf.Id = bbe.batter_id
    predictionDf.Season = bbe.season
    predictionDf.IsHomeRun = bbe.is_homerun

    # Set the variables for each type of model
    modelTypes = {
        'pHR': ['IsFlyBall', 'SprayAngle', 'IsBarrel'],
        'adjpHR': ['IsFlyBall', 'SprayAngle', 'IsBarrel', 'ParkFactor']
    }

    # If debugging run each model 10,000 times and take the average r2 of each result
    if args.debug:
        for modelType in modelTypes.keys():
            vars = modelTypes[modelType]
            print(vars)
            X_Filtered = X[vars].values
                
            scores = list()
            adjustedScores = list()

            for _ in range(100):
            # Split data into test and train sets
                X_train, X_test, Y_train, Y_test = train_test_split(X_Filtered, Y, shuffle=True, test_size=0.2)

                # Create and train model
                model = LinearRegression()
                model.fit(X_train, Y_train)

                # Test model
                score = (model.score(X_test, Y_test))
                scores.append(score)
                adjustedScores.append(adjustedR2(X_test, score))
            print("Average R2 {}".format(np.mean(scores)))
            print("Average Adjusted R2 {}".format(np.mean(adjustedScores)))

    # Create models using all of the data and make predictions
    for modelType in modelTypes.keys():
        vars = modelTypes[modelType]
        X_Filtered = X[vars].values
        model = LinearRegression()
        model.fit(X_Filtered, Y)
        predictionDf[modelType] = model.predict(X_Filtered)

    predictionDf.to_csv("PAVersion.csv")
    seasonDf = pd.DataFrame(columns=[
        'Id', 'Batter', 'Season', 'PA', 'HR', 'pHR', 'adjpHR', 'HR%', 'pHR%', 'adjpHR%'
    ])

    for id in set(predictionDf.Id):
        batterPredictions = predictionDf[predictionDf.Id == id] 
        for season in set(batterPredictions.Season):
            seasonPAs = paResults[(paResults.batter_id == id) & (paResults.season == season)]
            paCount = len(seasonPAs)
            if paCount < 200:
                continue
            batterSeason = batterPredictions[batterPredictions.Season == season]
            seasonDf = seasonDf.append({
                'Id' : id,
                'Batter' : seasonPAs.iloc[0].full_name,
                'Season' : season,
                'PA': paCount,
                'HR' : sum(batterSeason.IsHomeRun.astype(int)),
                'pHR' : sum(batterSeason.pHR),
                'adjpHR' : sum(batterSeason.adjpHR)
            }, ignore_index=True)
    
    print("Full season pHR r2 {}".format(r2_score(seasonDf.HR, seasonDf.pHR)))
    print("Full season adjpHR r2 {}".format(r2_score(seasonDf.HR, seasonDf.adjpHR)))

    seasonDf['HR%'] = seasonDf.HR / seasonDf.PA
    seasonDf['pHR%'] = seasonDf.pHR / seasonDf.PA
    seasonDf['adjpHR%'] = seasonDf.adjpHR / seasonDf.PA
    if args.debug:
        seasonDf.to_csv("PAVersion_Season.csv", index=False)

def main():
    # Parse arguments
    args = parseArgs()
    runByPlateAppearenceModel(args)

if __name__ == "__main__":
    main()